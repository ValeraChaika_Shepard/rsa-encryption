﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Numerics;

namespace protect_inf_LR1
{
    public partial class Form1 : Form
    {
        public Form1() { InitializeComponent(); }

        private void шиврованиеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ((textBox_p.Text.Length > 0) && (textBox_q.Text.Length > 0))
            {
                long p = Convert.ToInt64(textBox_p.Text);
                long q = Convert.ToInt64(textBox_q.Text);

                if (IsTheNumberSimple(p) && IsTheNumberSimple(q))
                {
                    string s = "";

                    StreamReader sr = new StreamReader("fileToEncode.txt");

                    while (!sr.EndOfStream)
                    {
                        s += sr.ReadLine();
                        s += "\n";
                    }

                    sr.Close();

                    long n = p * q;
                    long m = (p - 1) * (q - 1);
                    long d = Calculate_d(m);
                    long e_ = Calculate_e(d, m);
                    Console.WriteLine("AAAAa = " + e_);

                    List<string> result = encoder(s, e_, n);

                    StreamWriter sw = new StreamWriter("needToDecode.txt");
                    foreach (string item in result)
                        sw.WriteLine(item);
                    sw.Close();

                    textBox_d.Text = d.ToString();
                    textBox_n.Text = n.ToString();

                    Process.Start("notepad++.exe", "needToDecode.txt");
                }
                else
                    MessageBox.Show("p или q - не простые числа!");
            }
            else
                MessageBox.Show("Введите p и q!");
        }

        private void дешифрованиеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ((textBox_d.Text.Length > 0) && (textBox_n.Text.Length > 0))
            {
                long d = Convert.ToInt64(textBox_d.Text);
                long n = Convert.ToInt64(textBox_n.Text);

                List<string> input = new List<string>();

                StreamReader sr = new StreamReader("needToDecode.txt");

                while (!sr.EndOfStream)
                {
                    input.Add(sr.ReadLine());
                }

                sr.Close();

                string result = decoder(input, d, n);

                StreamWriter sw = new StreamWriter("decoded.txt");
                sw.WriteLine(result.Substring(0,result.Length-1));
                sw.Close();

                Process.Start("notepad++.exe", "decoded.txt");
            }
            else
                MessageBox.Show("Введите секретный ключ!");
        }

        //проверка: простое ли число?
        private bool IsTheNumberSimple(long n)
        {
            if (n < 2)
                return false;
            if (n == 2)
                return true;

            for (long i = 2; i < n; i++)
                if (n % i == 0)
                    return false;
            return true;
        }

        //шифрует
        private List<string> encoder(string s, long e, long n)
        {
            List<string> result = new List<string>();
            BigInteger bi;

            for (int i = 0; i < s.Length; i++)
            {
                //int index = Array.IndexOf(characters, s[i]);
                int charNumber = (int)s[i];

                bi = new BigInteger(charNumber);
                bi = BigInteger.Pow(bi, (int)e);

                BigInteger n_ = new BigInteger((int)n);

                bi = bi % n_;

                result.Add(bi.ToString());
            }
            return result;
        }

        //дешефрует
        private string decoder(List<string> inputMessage, long d, long n)
        {
            string result = "";
            BigInteger bi;

            foreach (string item in inputMessage)
            {
                bi = new BigInteger(Convert.ToDouble(item));
                bi = BigInteger.Pow(bi, (int)d);

                BigInteger n_ = new BigInteger((int)n);

                bi = bi % n_;

                int charNumber = Convert.ToInt32(bi.ToString());

                result += (char)charNumber; //characters[index].ToString();
            }

            return result;
        }

        //вычисление параметра d. d должно быть взаимно простым с m
        private long Calculate_d(long m)
        {
            long d = m - 1;

            for (long i = 2; i <= m; i++)
                //если имеют общие делители
                if ((m % i == 0) && (d % i == 0)) 
                {
                    d--;
                    i = 1;
                }

            return d;
        }

        //вычисление параметра e
        private long Calculate_e(long d, long m)
        {
            long e = 10;

            while (true)
            {
                if ((e * d) % m == 1)
                    break;
                else
                    e++;
            }
            return e;
        }

        private void списокПростыхЧиселToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StreamReader sr = new StreamReader("simpleNumbers.txt");
            string simplNumbs = sr.ReadToEnd();
            MessageBox.Show(simplNumbs,"Не благодарите)");
        }
    }
}
